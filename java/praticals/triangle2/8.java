/*
 E
 F G
 H I J
 K L M N
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row");
	int Row=Integer.parseInt(Br.readLine());
	int ch=65+Row;
	for(int i=1;i<=Row;i++){
		for(int j=1;j<=i;j++){
			System.out.print((char)ch++ + " ");
		}
		System.out.println();
	}
	}
}

/*
 1 
 1 c
 1 e 3
 1 g 3 i
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row");
	int Row=Integer.parseInt(Br.readLine());
	char ch='a';
	for(int i=1;i<=Row;i++){
		int num=1;
		for(int j=1;j<=i;j++){
			if(j%2==0){
				//ch+=2;
				System.out.print((ch+=2) +" ");
				num++;
			}
			else{
				System.out.print(num++ + " ");
			}
		}
		System.out.println();
	}
	}

}
 

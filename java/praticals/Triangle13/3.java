/*
 Row=5
 30  28  26  24  22
 20  18  16  14 
 12  10  8 
 6   4
 2
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Rows :");
	int Row=Integer.parseInt(Br.readLine());
	int num=Row*Row+Row;
	for(int i=1;i<=Row;i++){
		for(int j=Row;j>=i;j--){
			System.out.print(num +" ");
			num-=2;
		}
		System.out.println();
	}
	}
}

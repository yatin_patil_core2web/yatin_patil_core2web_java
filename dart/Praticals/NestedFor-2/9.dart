/*
1
2  6
3  7   10
4  8   11  13
*/
import "dart:io";
void main(){
	int? row=int.parse(stdin.readLineSync()!);
	
	for(int i=1;i<=row;i++){
		int inc=1;
		int num=i;
		for(int j=1;j<=i;j++){
			if(j>=3){
				
				stdout.write("${num-inc} ");
				inc=inc+2;
				num=num+row;
				
			}
			else{
				stdout.write("$num ");
				num=num+row;
			}
		}
		print(" ");
	}
}

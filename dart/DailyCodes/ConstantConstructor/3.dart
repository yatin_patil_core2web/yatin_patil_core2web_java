// Default/Optional Paramater
class Demo{
	
	final int? jerNo;
	final String? pName;
	final double? runRate;
	
	const Demo(this.jerNo,this.pName,[this.runRate=9.8]);

	void Info(){
		print(jerNo);
		print(pName);
		print(runRate);
	}
}

void main(){
	
	Demo obj1=const Demo(18,"Virat",7.8);
	Demo obj2=const Demo(7,"MSD");
	

	obj1.Info();
	obj2.Info();
	
}

/* row=3
       1
   1   2   1
1  2   3   2   1
*/

import "dart:io";
void main(){

	print("Enter the row");
	int? row=int.parse(stdin.readLineSync()!);
	//int num=0;
	for(int i=1;i<=row;i++){
		int num=1;
		for(int sp=1;sp<=row-i;sp++){
			stdout.write("\t");
		}
		
		for(int j=1;j<=i*2-1;j++){

			if(i>j){
				stdout.write("${num++}\t");
			}
			else{
				stdout.write("${num--}\t");
			}
		}
	
		print(" ");
	
		
	}
}

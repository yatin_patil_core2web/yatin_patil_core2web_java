/*
 1 
 2  4
 3  6
 4  8  12  16
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Rows :");
	int Row=Integer.parseInt(br.readLine());
	for(int i=1;i<=Row;i++){
		int num=i;
		for(int j=1;j<=i;j++){
			System.out.print(num +" ");
			num=num+i;
		}
		System.out.println();
	}
	}
	
}

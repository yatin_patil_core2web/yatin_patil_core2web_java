// To check whether the given number is an armstrong number or not

import "dart:io";
void main(){
	int? num=int.parse(stdin.readLineSync()!);
	int digit=0;
	int temp=num;
	while(temp>0){
		int rem=temp%10;
		digit++;
		temp~/=10;
	}
	int sum=0;
	int temp2=num;
	while(temp2>0){
		int rem2=temp2%10;
		int fact=1;
		for(int i=1;i<=digit;i++){
			fact=fact*rem2;
		}
		sum=sum+fact;
		temp2~/=10;
	}
	if(sum==num){
		print("$num is an Armstrong number");
	
	}	
	else{
		print("$num is not an Armstrong number ");
	}
}

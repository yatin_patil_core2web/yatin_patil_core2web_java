/*
 4  3  2  1
 3  2  1
 2  1 
 1
 */
import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Row :");
		int Row=sc.nextInt();
		for(int i=1;i<=Row;i++){
			for(int j=Row-i+1;j>=1;j--){
				System.out.print(j +" ");
			}
			System.out.println();
		}
	}

}

mixin parent1{

	int x=10;
	void fun(){
		print("In parent1 fun");
	}
}
mixin parent2{

	int x=20;
	void fun(){
		print("In parent2 fun");
	}
}
class child with parent1,parent2{


}
void main(){

	child obj=child();
	print(obj.x);
	obj.fun();
}

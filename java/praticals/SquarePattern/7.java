/*
 A  4  A		4  A  6  A
 6  B  8		8  B  10  B
 C  10  C		12  C  14  C
 			16  D  18   D
 */
import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Row :");
	int row=sc.nextInt();
	int num=row;
	for(int i=1;i<=row;i++){
		int ch=64+i;
		for(int j=1;j<=row;j++){
			if(num%2==1){
				System.out.print((char)ch +" ");
			}
			else{
				System.out.print(num +" ");
			}
			num++;
		}
		System.out.println();
	}
	}
}

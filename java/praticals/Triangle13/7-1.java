/*
 4  c  2  a
 3  b  1
 2  a 
 1
 */
import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Row :");
		int Row=sc.nextInt();
		for(int i=1;i<=Row;i++){
			int ch=97+Row-i;
			int num=Row;
			for(int j=Row-i+1;j>=1;j--){
				if(num%2==0){ 
					System.out.print(j +" ");
					ch--;
				}
				else{
					System.out.print((char)ch-- +" ");
				}
			num-=1;
			}
			
			System.out.println();
		}
	}

}

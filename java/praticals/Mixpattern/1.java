/*
 1  2  3  4
 5  6  7  8
 9  10 11 12
 13  14  15  16
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Rows : ");
	int Row=Integer.parseInt(br.readLine());
	int num=1;
	for(int i=1;i<=Row;i++){
		for(int j=1;j<=Row;j++){
			System.out.print(num++ +" ");
		}
		System.out.println();
	}
	}
}



class parent{

	parent(){
		
		print("In parent constructor");

	}
	void call(){
		print("In parent call");
	}

}
class child extends parent{

	child(){
		super();
		print("In child constructor");
	}
	
}
void main(){

	child obj=child();
	
	//parent obj=parent();
	
}

abstract class parent1{

	void marry(){
		print("In parent1 marry");
	}
}
abstract class parent2{

	void marry(){
		print("In parent2 marry");
	}
}
class child implements parent1,parent2{


}

void main(){
	child obj=child();
}
//This cod will bw error because child class do not define the body of the parent1 and parent2 class methods

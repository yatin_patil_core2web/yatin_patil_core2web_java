/*
 1  a  2  b
 1  a  2
 1  a
 1
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Rows : ");
	int Row=Integer.parseInt(br.readLine());
	for(int i=1;i<=Row;i++){
		int ch1=1;
		char ch2='a';
		for(int j=Row;j>=i;j--){
			if(j%2==0){
				System.out.print(ch1++ + " ");
			}
			else{
				System.out.print(ch2++ + " ");
			}
		}
		System.out.println();
	}
	}

}

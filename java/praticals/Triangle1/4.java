/*
 Row =4
 4 
 4  8  
 4  8  12
 4  8  12  16
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Row : ");
		int Row=Integer.parseInt(Br.readLine());
		for(int i=1;i<=Row;i++){
			int num=Row;
			for(int j=1;j<=i;j++){
				System.out.print(num +" ");
				num+=Row;
			}
		System.out.println();
		}
	
	}


}

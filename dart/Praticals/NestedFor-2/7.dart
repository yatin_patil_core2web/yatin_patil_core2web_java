/*
3
2  4
1  2  3
*/
import "dart:io";
void main(){
	int? row=int.parse(stdin.readLineSync()!);
	
	for(int i=row;i>=1;i--){
		int num=i;
		for(int j=row;j>=i;j--){
			stdout.write("$num ");
			num=num+i;
		}
		print(" ");
	}
}

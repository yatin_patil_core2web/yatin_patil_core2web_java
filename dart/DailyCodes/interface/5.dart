class parent1{
	parent1():super(){
		print("In parent1 constructor");
	}
}
class parent2{
	parent2():super(){
		print("In parent2 constructor");
	}
}
class child1 extends parent1{
	child1():super(){
		print("In child1 constructor");
	}
}
class child2 implements parent1,parent2{

	child2():super(){
		print("In child2 constructor");
	}
}
void main(){
	//child1 obj1=child1();
	child2 obj2=child2();

}

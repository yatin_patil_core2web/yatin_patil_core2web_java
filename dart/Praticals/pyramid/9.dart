/* row=4
 1  2  3  4  5  6  7
    8  9  10 11 12
      13  14 15
	  16
*/

import "dart:io";
void main(){

	print("Enter the row :");
	int? row=int.parse(stdin.readLineSync()!);
	int num=1;
	for(int i=row;i>=1;i--){
		
		for(int sp=1;sp<=row-i;sp++){

			stdout.write("\t");
		}

		for(int j=1;j<=i*2-1;j++){
			stdout.write("$num\t");
			num++;
		}
		print(" ");
	}
}


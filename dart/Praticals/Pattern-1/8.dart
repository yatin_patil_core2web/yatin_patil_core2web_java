// 1  2  3
// 2  3  4
// 3  4  5
import "dart:io";
void main(){

	int? row=int.parse(stdin.readLineSync()!);
	int? col=int.parse(stdin.readLineSync()!);
	int x=1;
	for(int i=1;i<=row;i++){
		
		for(int j=1;j<=col;j++){
			stdout.write("$x ");
			x++;
		}
		x=x-row+1;
		print(" ");
	}


}

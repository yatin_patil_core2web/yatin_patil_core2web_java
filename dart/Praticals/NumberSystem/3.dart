// To check whether the given number is a strong number  or not
import "dart:io";
void main(){

	int? num=int.parse(stdin.readLineSync()!);
	int temp=num;
	
	int sum=0;
	while(temp>0){
		int fact=1;
		int rem=temp%10;
		while(rem>0){
			fact=fact*rem;
			rem--;
		}
		sum=sum+fact;
		temp=temp~/10;
	}
	if(sum==num){
		print("$num is a strong number");
	
	}
	else{
		print("$num is not a strong number");
	}
}

class parent{

	parent(){
		print("In parent constructor");
	}
}
class child extends parent{

	child(){
		print("In child constructor");
		this();
	}
	void call(){
		print("In call child");
	}
}
void main(){
	child obj=child();
}

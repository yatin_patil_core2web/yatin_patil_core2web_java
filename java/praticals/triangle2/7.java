/*
 c
 C B
 c b a
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Row : ");
		int Row=Integer.parseInt(Br.readLine());
		for(int i=1;i<=Row;i++){
			int ch1=64+Row;
			int ch2=96+Row;
			for(int j=1;j<=i;j++){
				if(i%2==0){
					System.out.print((char)ch1-- + " ");
				}
				else{
					System.out.print((char)ch2-- +" ");
				}
			}
			System.out.println();
		
		}

}

}

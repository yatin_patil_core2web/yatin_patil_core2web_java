/*
 Rows=4
 2   4   6   8
 10  12  14 
 16  18
 20
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Rows :");
	int Row=Integer.parseInt(br.readLine());
	int num=2;
	for(int i=1;i<=Row;i++){
		for(int j=Row;j>=i;j--){
			System.out.print(num +" ");
			num+=2;
		}
		System.out.println();
	 }
	}
}

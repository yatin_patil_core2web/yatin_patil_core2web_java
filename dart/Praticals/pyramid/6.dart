/*
row=3
 
       0
   1   0   1
2  1   0   1  2


*/
import "dart:io";
void main(){
	
	print("Enter the row: ")	;
	int? row=int.parse(stdin.readLineSync()!);
	
	for(int i=1;i<=row;i++){
		int num=i-1;

		for(int sp=1;sp<=row-i;sp++){
			stdout.write("\t");
		}
		
		for(int j=1;j<=i*2-1;j++){
			if(i>j){
				stdout.write("${num--}\t");
			}
			else{
				stdout.write("${num++}\t");
			}
		}
		print(" ");
	}

}

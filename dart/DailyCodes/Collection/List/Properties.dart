void main(){

	List player=["Virat","Sachin","Rohit","MSD"];
	print(player);
	print(player.runtimeType);
	
	//first
	print(player.first); // virat
	
	//last
	print(player.last); // MSD

	//Length
	print(player.length); //4

	//isEmpty
	print(player.isEmpty);//false

	//isNotEmpty
	print(player.isNotEmpty); // True

	//reversed
	print(player.reversed); //[msd,rohit,sachin,virat]
}

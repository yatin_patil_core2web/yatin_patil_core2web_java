/*
 1 
 2 a
 3 b 3
 4 c 4 d
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row");
	int Row=Integer.parseInt(Br.readLine());
	int num=1;
	char ch='a';
	for(int i=1;i<=Row;i++){
		for(int j=1;j<=i;j++){
			if(j%2==0){
				System.out.print(ch++ + " ");
			}
			else{
				System.out.print(num + " ");
			}
		
		}
		num++;
		System.out.println();
	}
	}
}

/*
 A  B  C
 a  b
 A
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Rows : ");
	int Row=Integer.parseInt(br.readLine());
	for(int i=1;i<=Row;i++){
		char ch1='A';
		char ch2='a';
		for(int j=Row;j>=i;j--){
			if(i%2==1){
				System.out.print(ch1++ + " ");
			}
			else{
				System.out.print(ch2++ + " ");
			}
		}
		System.out.println();
	}
	}

}

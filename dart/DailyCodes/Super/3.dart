class parent{
	
	int? x;
	int? y;
	
	parent(this.x,this.y){
		
		print("In parent constructor");
		print(x);
		print(y);
	}

}
class child extends parent{
	
	child(int x,int y):super(x,y){
		print("In child constructor");
		print(x);
		print(y);
	}
}
void main(){

	child obj=child(10,20);
	
}

/*
 E  D  C  B  A
 E  D  C  B
 E  D  C
 E  D  
 E
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row : ");
	int Row=Integer.parseInt(Br.readLine());
	for(int i=1;i<=Row;i++){
		int ch=64+Row;
		for(int j=Row;j>=i;j--){
			System.out.print((char)ch-- + " ");
		}
		System.out.println();
	}
	}

}

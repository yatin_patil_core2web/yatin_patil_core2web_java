class parent{

	int x=10;
	parent(){
		print("In parent constructor");
		print(x);
		fun();
	}
	void fun(){
		print("In fun parent");
	}

}
class child extends parent{
	
	int x=20;
	child(){
		print("In child constructor");
		print(x);
		fun();
	}
	void fun(){
		print("In child fun");
	}	

}
void main(){
	child obj=child();
}

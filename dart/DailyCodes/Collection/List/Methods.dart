void main(){

	List jer=[10,20,30,40];
	print(jer);
	print(jer.runtimeType);
	
	//add()
	jer.add(50);
	print(jer); //[10,20,30,40,50]

	//insert(index,value)
	jer.insert(2,15);
	print(jer); //[10,20,15,30,40,50]
	
	//insertAll(index,list)
	jer.insertAll(1,[1,2,3,4,15]);
	print(jer);
	
	//indexof
	print(jer.indexOf(15));

	//lastIndexOf()
	print(jer.lastIndexOf(15));
	
	//remove(element)
	print(jer.remove(15));
	print(jer);
	
	// removeLast	
	jer.removeLast();
	print(jer);

	//removeRange(start,end-1);
	jer.removeRange(2,5);
	print(jer);
}

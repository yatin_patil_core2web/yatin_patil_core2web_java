void main(){

	Set data={1,2,3,4,5,6};
	
	//first
	print(data.first);
	
	//last
	print(data.last);
	
	//length
	print(data.length);
	
	//isEmpty
	print(data.isEmpty);
	
	//isNotEmpty
	print(data.isNotEmpty);
	
	//reversed
	//print(data.reversed);
}

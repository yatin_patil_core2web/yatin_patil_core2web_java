abstract class parent1{
	
	void fun();
}
abstract class parent2{

	void fun();
}

mixin Demo on parent1{
	void fun(){

		print("mixin fun from  parent1 class  ");
	}
}
class child extends parent2 with Demo{

}

void main(){
	child obj=child();
	obj.fun();
}


// Named Paramater
class Demo{
	
	final int? jerNo;
	final String? pName;
	final double? runRate;
	
	const Demo({this.jerNo,this.pName,this.runRate});

	void Info(){
		print(jerNo);
		print(pName);
		print(runRate);
	}
}

void main(){
	
	Demo obj1=const Demo(jerNo:18,pName:"Virat",runRate:7.8);
	Demo obj2=const Demo(pName:"MSD",jerNo:7);
	

	obj1.Info();
	obj2.Info();
	
}

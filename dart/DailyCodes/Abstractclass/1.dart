abstract class parent{
	void marray();
	void food(){
		print("In parent food");
	}
}
class child extends parent{
	void marray(){
		print("In marray child");
	}
	void food(){
		print("In food child");
	}

}
void main(){

	child obj=child();
	obj.food();
}

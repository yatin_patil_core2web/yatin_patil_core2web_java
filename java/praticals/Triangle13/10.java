/*
 Row=3
 c  b  a
 B  A
 a

 Row=4
 D  C  B  A
 c  b  a
 B  A
 a
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row : ");
	int Row=Integer.parseInt(br.readLine());
	for(int i=Row;i>=1;i--){
		int ch1=64+i;
		int ch2=96+i;
		for(int j=1;j<=i;j++){
			if(i%2==0){
				System.out.print((char)ch1-- +" ");
			}
			else{
				System.out.print((char)ch2-- +" ");
			}
		}
		System.out.println();
	}
	}

}

/*
 C  4  5		D  5  6  7
 F  7  8		H  9  10 11
 I  10  11		L  13  14  15
 			P  17  18  19
 */
import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Row : ");
	int row=sc.nextInt();
	int num=row;
	for(int i=1;i<=row;i++){
		int ch=64+num;
		for(int j=1;j<=row;j++){
			if(num%row==0){
				System.out.print((char)ch +" ");
			}
			else{
				System.out.print(num +" ");
			}
			num++;
		}
		System.out.println();

	
	}
	}
}

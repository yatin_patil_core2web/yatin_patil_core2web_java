class parent{

	
	static int x=10;
	static void fun(){
		print("In static fun from parent");
	}

}
class child implements  parent{
	int x=20;
	void fun(){
		print("In fun child");
		print(x);	// in child class x
		print(parent.x);	// parent class x
		parent.fun();
	}
	
}
void main(){

	child obj=child();
	obj.fun();


} 

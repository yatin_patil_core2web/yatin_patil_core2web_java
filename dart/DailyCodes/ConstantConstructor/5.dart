// Mixed paramter
class Demo{
	
	final int? jerNo;
	final String? pName;
	final double? runRate;
	
	const Demo({this.jerNo=20,this.pName,this.runRate=9.5});

	void Info(){
		print(jerNo);
		print(pName);
		print(runRate);
	}
}

void main(){
	
	Demo obj1=const Demo(jerNo:18,pName:"Virat",runRate:7.8);
	Demo obj2=const Demo(pName:"MSD");
	

	obj1.Info();
	obj2.Info();
	
}

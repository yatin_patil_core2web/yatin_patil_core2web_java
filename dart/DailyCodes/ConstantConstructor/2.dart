// Positional Paramater
class Demo{
	
	final int? jerNo;
	final String? pName;
	final double? runRate;
	
	const Demo(this.jerNo,this.pName,this.runRate);

	void Info(){
		print(jerNo);
		print(pName);
		print(runRate);
	}
}

void main(){
	
	Demo obj=const Demo(18,"Virat",7.8);
	obj.Info();
	
}

/*
 3
 3  2  
 3  2  1
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Rows : ");
		int Row=Integer.parseInt(Br.readLine());
		for(int i=1;i<=Row;i++){
			int num=Row;
			for(int j=1;j<=i;j++){
				System.out.print(num-- +" ");
			}
			System.out.println();
		}
	}
}

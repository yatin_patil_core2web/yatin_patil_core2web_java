/*
 4  3  2  1
 3  2  1
 2  1
 1
 */
import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Row");
	int Row=sc.nextInt();
	for(int i=Row;i>=1;i--){
		int num=i;
		for(int j=1;j<=i;j++){
			System.out.print(num-- + " ");
		}
		System.out.println();
	}
	}
}

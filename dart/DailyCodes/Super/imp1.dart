class parent{

	int x=10;
	parent(){
		print("In parent constructor");
		print(x);
	}

}
class child extends parent{
	
	int x=20;
	child(){
		print("In child constructor");
		print(x);
	}	

}
void main(){
	child obj=child();
}

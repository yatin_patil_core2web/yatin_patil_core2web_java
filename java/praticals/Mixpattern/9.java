/*
 1  2 3  4
 C  B  A
 1  2
 A
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row :");
	int Row=Integer.parseInt(br.readLine());
	for(int i=Row;i>=1;i--){
		int num=1;
		int ch=64+i;
		for(int j=1;j<=i;j++){
			if(i%2==0){
				System.out.print(num++ +" " );
			}
			else{
				System.out.print((char)ch-- +" ");
			}
		}
		System.out.println();
	}
	}
}

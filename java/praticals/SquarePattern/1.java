/*
 C   B  A		D  C  B  A
 3   3  3		4  4  4  4
 C   B  A		D  C  B  A
 			4  4  4  4
 */	
import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Rows : ");
	int row=sc.nextInt();
	for(int i=1;i<=row;i++){
		int ch=64+row;
		int num=row;
		for(int j=1;j<=row;j++){
			if(i%2==1){
				System.out.print((char)ch-- +" ");
			}
			else{
				System.out.print(num +" ");
			}

		}
		System.out.println();
	}
	
	}

}

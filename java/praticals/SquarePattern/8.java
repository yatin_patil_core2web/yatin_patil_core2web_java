/*
 #  C  #		#  D  #  C
 C  #  B		D  #  C  #
 #  C  #		#  D  #  C
			D  #  C  #
 */
import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Rows :");
	int row=sc.nextInt();
	for(int i=1;i<=row;i++){
		int ch=64+row;
		for(int j=1;j<=row;j++){
			if(i%2==1){
				if(j%2==1){
					System.out.print("#" +" ");
				}
				else{
					System.out.print((char)ch-- +" ");
				}
			}
			else{
				if(j%2==1){
					System.out.print((char)ch-- +" ");
				}
				else{
					System.out.print("#" +" ");
				}
			}
		}
		System.out.println();
	}
     }
	}
	


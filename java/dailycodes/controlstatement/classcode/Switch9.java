class SwitchDemo{

	public static void main(String[] args){
	
	float num=1.5f;
	System.out.println("before Switch");
	switch(num){
	
		case 1.5:
			System.out.println("1.5");
			break;
		case 2.0:
			System.out.println("2.0");
			break;
		case 3.4:
			System.out.println("3.4");
			break;
		default:
			System.out.println("In Default state");
	
	}
	
	
	}



}

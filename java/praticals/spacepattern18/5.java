 
//        1
//     2  4
//  3  6  9
//4 8  12  16
import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Rows :");
	int row=sc.nextInt();
	for(int i=1;i<=row;i++){
		for(int sp=1;sp<=row-i;sp++){
			System.out.print(" "+ " ");
		}
		int num=i;
		for(int j=1;j<=i;j++){
			System.out.print(num +" ");
			num=num+i;
		}
		System.out.println();
	}
	}

}

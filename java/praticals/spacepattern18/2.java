 
//        3
//     3  2
//  3  2  1
import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Rows :");
	int row=sc.nextInt();
	for(int i=1;i<=row;i++){
		for(int sp=1;sp<=row-i;sp++){
			System.out.print(" "+ " ");
		}
		int num=row;
		for(int j=1;j<=i;j++){
			System.out.print(num-- +" ");
		}
		System.out.println();
	}
	}

}

// Write a program to Print the Factorial of the number
// input 5=120
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the number :  ");
	int num=Integer.parseInt(Br.readLine());
	int product=1;
	int temp=1;
	while(num>=temp){
		product=product*num;
		num--;
	}
	System.out.println("factorial is : "+ product);
	}
}

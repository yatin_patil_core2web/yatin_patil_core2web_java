/*
 4
 3  6
 2  4  6
 1  2  3  4
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row : ");
	int Row=Integer.parseInt(br.readLine());
	for(int i=Row;i>=1;i--){
		int num=i;
		for(int j=Row;j>=i;j--){
			System.out.print(num +" ");
			num=num+i;
		}
		System.out.println();
	}
	
	}
}

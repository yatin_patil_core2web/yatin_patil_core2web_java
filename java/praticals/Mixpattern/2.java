/*
 C3  C2  C1
 C4  C3  C2
 C5  C4  C3
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row : ");
	int Row=Integer.parseInt(br.readLine());
	int num=Row;
	int ch=64+Row;
	for(int i=1;i<=Row;i++){
		for(int j=1;j<=Row;j++){
			System.out.print((char)ch +""+ num-- +" ");
		}
		num=num+Row+1;
		System.out.println();

	}
	}
}

/*
 1  2  3  4
 1  2  3
 1  2
 1
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row");
	int Row=Integer.parseInt(Br.readLine());
	for(int i=1;i<=Row;i++){
		int num=1;
		for(int j=Row;j>=i;j--){
			System.out.print(num++ +" ");
		}
		System.out.println();
	}
	}

}

abstract class parent{
	
	void marray();
	parent():super(){
		print("In parent constructor");
	}

}
class child extends parent {
	
	void marray(){
		print("In marray child");
	}
	child():super(){
		print("In child constructor");
	}


}
void main(){
	child obj=child();
	obj.marray();
}

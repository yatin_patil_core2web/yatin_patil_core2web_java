/*
 3  B  1		4  C  2  A
 C  B  A		D  C  B  A
 3  B  1		4  C  2  A
 			D  C  B  A
 */





import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Rows :");
	int Row=sc.nextInt();
	for(int i=1;i<=Row;i++){
	int num=Row;
	int ch=64+Row;
	for(int j=1;j<=Row;j++){
		if(i%2==1){
			if(j%2==1){
				System.out.print(num-- +" ");
				ch--;
			}
			else{
				System.out.print((char)ch +" ");
				ch--;
				num--;
			}
		}
		else{
			System.out.print((char)ch-- +" ");
		}
	}
	System.out.println();
	}
	
	}

}

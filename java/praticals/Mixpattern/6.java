/*
 d  
 4  3
 d  c  b
 4  3  2  1
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Rows : ");
	int Row=Integer.parseInt(br.readLine());
	for(int i=1;i<=Row;i++){
		int num=Row;
		int ch=96+Row;
		for(int j=1;j<=i;j++){
			if(i%2==1){
				System.out.print((char)ch-- +" ");
			}
			else{
				System.out.print(num-- +" ");
			}
		}
		System.out.println();
	
	}
	}
}

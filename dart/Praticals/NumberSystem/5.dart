// To check whether the given number is a palindrome number or not
import "dart:io";
void main(){
	int? num=int.parse(stdin.readLineSync()!);
	int temp=num;
	int rev=0;
	while(temp>0){
		int rem=temp%10;
		rev=rem+rev*10;
		temp~/=10;
		 
	}
	if(rev==num){
		print("$num is a palindrome Number");
	}
	else{
		print("$num is Not  a palindrome Number");
	
	}
}

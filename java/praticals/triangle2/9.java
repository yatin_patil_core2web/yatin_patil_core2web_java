/*
 1
 B C 
 1 2 3 
 G H I J
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row : ");
	int Row=Integer.parseInt(Br.readLine());
	char ch='A';
	for(int i=1;i<=Row;i++){
		int num=1;
		for(int j=1;j<=i;j++){
			if(i%2==1){
				System.out.print(num++ + " ");
				ch++;

			}
			else{
				System.out.print(ch++ +" ");
			}
		}
		System.out.println();
	}
	}
}

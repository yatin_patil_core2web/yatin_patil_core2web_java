//Write a program to check whether given number is Prime or not
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the number : ");
	int num=Integer.parseInt(br.readLine());
	int count=0;
	int temp=1;
	while(temp<=num){
		if(num%temp==0){
			count++;
		}
		temp++;
	}
	System.out.println("Count is : "+ count);
	if(count==2){
		System.out.println("This number is Prime Number : ");
	}
	else{
		System.out.println("This number is not Prime number :");
	}
	}

}

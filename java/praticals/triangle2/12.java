/*
 4		5
 4 a		5 a
 4 b 6		5 b 7
 		5 c 7 d
 */	

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row : ");
	int Row=Integer.parseInt(Br.readLine());
	char ch='a';
	for(int i=1;i<=Row;i++){
		int num=Row+1;
		for(int j=1;j<=i;j++){
			if(j%2==1){
				System.out.print(num +" ");
				num+=2;
			}
			else {
				System.out.print(ch++ +" ");
			}
		}
		System.out.println();
	}
	}
}

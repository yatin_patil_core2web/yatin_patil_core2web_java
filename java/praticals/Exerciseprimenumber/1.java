//Write a program to print the Factors of the given number 
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the number : ");
	int num=Integer.parseInt(br.readLine());
	int temp=1;
	while(temp<=num){
		if(num%temp==0){
			System.out.println(temp);
		}
		temp++;
	}
	
	}

}

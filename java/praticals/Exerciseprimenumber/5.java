// Write a program to reverse the given number 
//7853 is 3587
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number : ");
		int num=Integer.parseInt(br.readLine());
		int temp=num;
		int rem=1;
		int var=0;
		while(temp>0){
			rem=temp%10;
			var=var*10+rem;
			temp/=10;
		}
		System.out.println(var);
	}

}


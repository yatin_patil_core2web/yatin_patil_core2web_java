/*
2  6  6		2  6   6  12
3  4  9 	3  4  9   8
2  6  6		2  6  6   12
		3  4  9   8

*/



import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Rows :");
	int row=sc.nextInt();
	for(int i=1;i<=row;i++){
		for(int j=1;j<=row;j++){
		if(i%2==1){
			if(j%2==1){
				System.out.print((j*2)+" ");
			}
			else{
				System.out.print((j*3) +" ");
			}
		}
		else{
			if(j%2==1){
				System.out.print((j*3)+" ");
			}
			else{
				System.out.print((j*2)+" ");
			}
		}
		}
		System.out.println();
	}
}
}

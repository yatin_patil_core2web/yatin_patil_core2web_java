class SwitchDemo{

	public static void main(String[] args){
	int num=45;
	num%=2;
	switch(num){
	
		case 0:
			System.out.println(num + " number is even");
			break;
		case 1:
			System.out.println(num + " number is odd");
			break;
		default:
			System.out.println("Number is out of range");
			break;
	
	}
	
	}

}

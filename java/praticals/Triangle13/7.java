/*
 4  c  2  a
 3  b  1
 2  a
 1
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Rows : ");
	int Row=Integer.parseInt(br.readLine());
	for(int i=Row;i>=1;i--){
		int num=i;
		int ch=96+i;
		for(int j=1;j<=i;j++){
			if(j%2==1){
				System.out.print(num-- +" ");
				ch--;
			}
			else{
				System.out.print((char)ch-- + " ");
			}
		}
		System.out.println();
	}
	}
}                                              


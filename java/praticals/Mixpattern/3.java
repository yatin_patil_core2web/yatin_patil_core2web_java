/*
 D  C  B  A
 1  2  3  4
 D  C  B  A
 1  2  3  4
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row : ");
	int Row=Integer.parseInt(br.readLine());
	for(int i=1;i<=Row;i++){
		int num=1;
		int ch=64+Row;
		for(int j=1;j<=Row;j++){
			if(i%2==0){
				System.out.print(num++ +" ");
			}
			else{
				System.out.print((char)ch-- +" ");
			}
		}
		System.out.println();
	}
	}
}

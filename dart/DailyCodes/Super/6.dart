class parent{
	
	int? x;
	int? y;
	
	parent({this.x=70,this.y=40}){
		print("In parent constructor");
		print(x);
		print(y);
		
	}

}
class child extends parent{

	child(int x,int y):super(){

		print("In child constructor");
		print(x);
		print(y);
	}

}
void main(){

	child obj=child(10,20);
}

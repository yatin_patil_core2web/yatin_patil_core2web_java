/*
 1 
 b  c 
 4  5  6
 g  h  i  j
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row : ");
	int Row=Integer.parseInt(Br.readLine());
	int num=1;
	char ch='a';
	for(int i=1;i<=Row;i++){
		for(int j=1;j<=i;j++){
			if(i%2==1){
				System.out.print(num + " ");
				num++;
				ch++;
			}
			else{
				System.out.print(ch++ +" ");
				num++;
			}
		}
		System.out.println();
	
	}
	}

}

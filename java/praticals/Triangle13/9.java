/*
 19  17  15  13
 11  9  7
 5  3
 1
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row : ");
	int Row=Integer.parseInt(br.readLine());
	int num=Row*Row+Row-1;
	for(int i=Row;i>=1;i--){
		for(int j=1;j<=i;j++){
			System.out.print(num +" ");
			num-=2;
		}
		System.out.println();
	}
	}
}

// Write a program to print the element of the array which is divisible by 4 .Take input from the User 
import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Size :");
	int size=sc.nextInt();
	int arr[]=new int[size];
	int sum=0;
	for(int i=0;i<arr.length;i++){
		arr[i]=sc.nextInt();
	}
	System.out.println("Display the even number form array : ");
	for(int j=0;j<arr.length;j++){
		if(arr[j]%4==0){
			System.out.println(arr[j] +" is divisible by 4 and its Index is " + j);
		}
	}
	
	
	}
}

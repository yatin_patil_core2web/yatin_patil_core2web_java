/*
 a
 $ $
 a b c

*/
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader Br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the Row");
	int Row=Integer.parseInt(Br.readLine());
	for(int i=1;i<=Row;i++){
		char ch='a';
		for(int j=1;j<=i;j++){
			if(i%2==1){
				System.out.print(ch++ +" ");
			}
			else{
				System.out.print("$ ");
			}
		}
		System.out.println();
	}

	}

}

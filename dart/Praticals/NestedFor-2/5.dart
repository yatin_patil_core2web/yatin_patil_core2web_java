/*
4
8   12
16  20  24
28  32  36   40
*/
import "dart:io";
void main(){
	
	int? row=int.parse(stdin.readLineSync()!);
	int num=row;
	for(int i=1;i<=row;i++){
		
		for(int j=1;j<=i;j++){
			stdout.write("$num  ");
			num=num+row;
		}
		print(" ");
	}

}

// Write a program where you hava to print the element from the array which are less than 10.Take input from user
import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the  size : ");
	int size=sc.nextInt();
	int arr[]=new int[size];
	System.out.println("Enter the element : ");
	for(int i=0;i<arr.length;i++){
		arr[i]=sc.nextInt();
	}
	System.out.println("Less than 10 element : ");
	for(int j=0;j<arr.length;j++){
		if(arr[j]<10){
			System.out.println(arr[j] +" is Less than 10");
		}
	}
	}
}
